.. licheck documentation master file, created by
   sphinx-quickstart on Wed Dec 27 17:32:50 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to licheck's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   external_programs
   configuration_file
   api
   pre_commit_hook
   contributing
   workflow
   source_code
   copyright_license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
