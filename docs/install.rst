Installation
============

Install licheck via pip:

::

    $ pip3 install licheck --user


All the necessary dependencies are installed automatically along with the
program.

Distribution packages
---------------------

- A ``PKGBUILD`` for Arch Linux like distributions is available under
  the ``./packages/aur`` directory as well as on the AUR website.

Dependencies
------------

- Python >= 3.5
- PyYAML_
- python-tabulate_
- appdirs_
- Requests_

.. _PyYAML: https://pyyaml.org/
.. _python-tabulate: https://github.com/astanin/python-tabulate
.. _appdirs: https://github.com/ActiveState/appdirs
.. _Requests: https://requests.readthedocs.io/
