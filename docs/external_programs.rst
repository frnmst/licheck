External programs
=================

Introduction
------------

licheck relies on external programs to check the licenses.

Supported markdown parsers
--------------------------

+------------------+--------------+-----------------------------------------+------------------------------+
| Language         | Program      | Homepage                                | Installation command         |
+------------------+--------------+-----------------------------------------+------------------------------+
| ``python``       | dep_license  | https://github.com/abduhbm/dep-license  | ``pip3 install dep_license`` |
+------------------+--------------+-----------------------------------------+------------------------------+
