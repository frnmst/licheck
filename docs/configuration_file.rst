Configuration file
==================

Local files
-----------

In the root of your repository create a file called ``.allowed_licenses.yml``.

There are two possible formats for this file:

1. use a local list of licenses directly in the file, for example:

   .. code-block:: yaml

		allowed licenses:
		    - OSI Approved::BSD License
		    - OSI Approved::Apache Software License
		    - OSI Approved::MIT License
		    - OSI Approved::GNU General Public License v3 or later (GPLv3+)

		files to check:
		    - Pipfile
		    - setup.py

		language: python

		include: []

2. include a remote file, for example:

   .. code-block:: yaml

		allowed licenses: []

		files to check:
		    - Pipfile
		    - setup.py

		language: python

		include:
		    - https://codeberg.org/frnmst/licheck/raw/branch/master/examples/python_gpl_compatible.yml


Remote files
------------

Remote files may only contain ``allowed licenses`` values.
The use of remote files is inspired
by `kontrolilo <https://github.com/kontrolilo/kontrolilo>`_ and
`license-check-config <https://github.com/nbyl/license-check-config>`_
, for example:

.. literalinclude:: ../examples/python_gpl_compatible.yml
   :language: yaml
