Developer Interface
===================

.. module:: licheck

Main Interface
--------------

Examples for the most relevant api functions can be viewed in the test
file. licheck's API uses `type hints`_ instead of assertions to check input and
output types.

.. _type hints: https://docs.python.org/3/library/typing.html

.. autofunction:: build_command
.. autofunction:: check_cache_structure
.. autofunction:: check_configuration_structure
.. autofunction:: check_data_object_structure
.. autofunction:: check_dependencies_files_data_structure
.. autofunction:: check_licenses
.. autofunction:: create_cache_output
.. autofunction:: create_data_object
.. autofunction:: create_dependencies_files_data_structure
.. autofunction:: get_binary_and_program
.. autofunction:: get_data
.. autofunction:: is_sha512_checksum
.. autofunction:: pipeline
.. autofunction:: prepare_print
.. autofunction:: print_errors
.. autofunction:: read_cache_file
.. autofunction:: read_configuration_file
.. autofunction:: read_remote_files
.. autofunction:: read_yaml_file
.. autofunction:: save_cache
.. autofunction:: transform_cache_to_data_object
.. autofunction:: write_cache

Exceptions
----------

.. autoexception:: BinaryDoesNotExist
.. autoexception:: IncoherentData
.. autoexception:: IncoherentProgrammingLanguageValue
.. autoexception:: InvalidCache
.. autoexception:: InvalidCommonDataStructure
.. autoexception:: InvalidConfiguration
.. autoexception:: InvalidOutput
.. autoexception:: NotAChecksum
