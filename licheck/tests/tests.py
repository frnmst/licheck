#!/usr/bin/env python3
#
# tests.py
#
# Copyright (C) 2021 Franco Masotti (franco DoT masotti {-A-T-} tutanota DoT com)
#
# This file is part of licheck.
#
# licheck is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# licheck is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with licheck.  If not, see <http://www.gnu.org/licenses/>.
#
r"""The tests module."""

import copy
import unittest
from unittest.mock import patch

import requests_mock
import yaml

from .. import api, exceptions
from ..constants import common_defaults


class SubprocessOutput:
    r"""An object representing the output of the subprocess.run method."""

    stdout = str()
    stderr = str()


class TestApi(unittest.TestCase):
    r"""Test the main API."""

    def test_build_command(self):
        r"""Test if the command string is build correctly."""
        # dep_license (v2.4.2).
        self.assertEqual(api.build_command('name', 'dep_license', 'file'), 'name --dev --format json file')

        self.assertEqual(api.build_command('name', 'unrecognized_command', 'file'), str())

    def test_create_data_object(self):
        r"""Test if the data object is built correctly."""
        self.assertEqual(api.create_data_object(list(), 'dep_license', 'file'), list())
        self.assertEqual(
            api.create_data_object(
                [
                    {
                        'Name': 'test',
                        'Meta': 'ls',
                        'Classifier': 'll',
                    }
                ], 'dep_license', 'file'
            ),
            [
                {
                    'package': 'test',
                    'license_short': ['ls'],
                    'license_long': ['ll'],
                    'file': 'file',
                    'version': str(),
                }
            ]
        )

        self.assertEqual(api.create_data_object(list(), 'unrecognized_program', 'file'), list())

    def test_get_data(self):
        r"""Test if the capture the output of subprocesses corresponds to expected objects."""
        ########################
        # dep_license (v2.4.2) #
        ########################
        s = SubprocessOutput()
        s.stdout = b'''
Found dependencies: 3

[
    {
        "Name": "twine",
        "Meta": "",
        "Classifier": "OSI Approved::Apache Software License"
    },
    {
        "Name": "requests",
        "Meta": "Apache 2.0",
        "Classifier": "OSI Approved::Apache Software License"
    },
    {
        "Name": "pre-commit",
        "Meta": "MIT",
        "Classifier": "OSI Approved::MIT License"
    }
]
        '''
        with patch('subprocess.run', return_value=s):
            self.assertEqual(
                api.get_data('fake_cmd', 'dep_license'),
                [
                    {
                        'Name': 'twine',
                        'Meta': str(),
                        'Classifier': 'OSI Approved::Apache Software License'
                    },
                    {
                        'Name': 'requests',
                        'Meta': 'Apache 2.0',
                        'Classifier': 'OSI Approved::Apache Software License'
                    },
                    {
                        'Name': 'pre-commit',
                        'Meta': 'MIT',
                        'Classifier': 'OSI Approved::MIT License'
                    }
                ]
            )

        s = SubprocessOutput()
        s.stdout = b'no dependencies found'
        with patch('subprocess.run', return_value=s):
            self.assertEqual(api.get_data('fake_cmd', 'dep_license'), dict())

        s = SubprocessOutput()
        s.stdout = b'any output'
        with patch('subprocess.run', return_value=s):
            with self.assertRaises(exceptions.InvalidOutput):
                api.get_data('fake_cmd', 'dep_license')

        ###################
        # Unknown program #
        ###################
        s = SubprocessOutput()
        s.stdout = b'any output'
        with patch('subprocess.run', return_value=s):
            self.assertEqual(api.get_data('fake_cmd', 'non_existing_license_program'), dict())

    def test_transform_cache_to_data_object(self):
        r"""Test transform_cache_to_data_object."""
        # Test a valid cache with file_checksum == id
        cache = {
            'a0' * 64: [
                {
                    'p': 'pkg0',
                    's': ['short'],
                    'l': ['long'],
                    'v': 'ver0',
                },
            ],
            'a1' * 64: [
                {
                    'p': 'pkg1',
                    's': ['short'],
                    'l': ['long'],
                    'v': 'ver12',
                },
            ],
        }
        # Test if invalid common data structure function is called.
        m = unittest.mock.Mock(return_value=True)
        with patch('licheck.api.check_cache_structure', m):
            self.assertEqual(
                api.transform_cache_to_data_object(cache, 'file', 'a0' * 64),
                [
                    {
                        'package': 'pkg0',
                        'license_short': ['short'],
                        'license_long': ['long'],
                        'file': 'file',
                        'version': 'ver0',
                    }
                ]
            )

        # Test a cache without corresponding file_checksum == id.
        cache = {
            'a1' * 64: [
                {
                    'p': 'pkg0',
                    's': ['short'],
                    'l': ['long'],
                    'v': 'ver0',
                },
            ],
        }
        m = unittest.mock.Mock(return_value=True)
        with patch('licheck.api.check_cache_structure', m):
            self.assertEqual(
                api.transform_cache_to_data_object(cache, 'file', 'a0' * 64),
                [
                ]
            )

        # Test if invalid common data structure function is called.
        m = unittest.mock.Mock(return_value=False)
        with patch('licheck.api.check_cache_structure', m):
            with self.assertRaises(exceptions.InvalidCache):
                api.transform_cache_to_data_object(dict(), str(), str())

    @unittest.skip("empty test")
    def test_read_yaml_file(self):
        r"""Test read_yaml_file."""

    @unittest.skip("empty test")
    def test_write_cache(self):
        r"""Test write_yaml_file."""

    def test_save_cache(self):
        r"""Test save_cache."""

    def test_create_cache_output(self):
        r"""Test create_cache_output."""
        # Test a valid structure.
        packages = [
            {
                'package': 'pkg0',
                'license_short': ['short'],
                'license_long': ['long'],
                'file': 'file',
                'version': 'ver0',
            },
            {
                'package': 'pkg1',
                'license_short': ['short'],
                'license_long': ['long'],
                'file': 'file',
                'version': 'ver1',
            },
        ]
        file_checksum = '0a' * 64
        table = dict()
        expected_table = {
            '0a' * 64:
            [
                {
                    'p': 'pkg0',
                    's': ['short'],
                    'l': ['long'],
                    'v': 'ver0',
                },
                {
                    'p': 'pkg1',
                    's': ['short'],
                    'l': ['long'],
                    'v': 'ver1',
                },
            ],
        }
        api.create_cache_output(packages, file_checksum, table)
        self.assertEqual(table, expected_table)

        # Test if invalid common data structure function is called.
        m = unittest.mock.Mock(return_value=False)
        with patch('licheck.api.check_data_object_structure', m):
            with self.assertRaises(exceptions.InvalidCommonDataStructure):
                api.create_cache_output(packages, file_checksum, table)
        m = unittest.mock.Mock(return_value=False)
        with patch('licheck.api.is_sha512_checksum', m):
            with self.assertRaises(exceptions.NotAChecksum):
                api.create_cache_output(packages, file_checksum, table)

    def test_check_licenses(self):
        r"""Test check_licenses."""
        # Test a valid structure.
        pkgs = [
            {
                'package': 'pkg0',
                'license_short': ['short'],
                'license_long': ['long'],
                'file': 'file',
                'version': 'ver0',
            }
        ]
        licenses_allowed = ['some license']
        errors = [pkgs[0]]
        self.assertEqual(api.check_licenses(pkgs, licenses_allowed), errors)

        # Test multiple packages.
        pkgs = [
            {
                'package': 'pkg0',
                'license_short': ['short'],
                'license_long': ['long'],
                'file': 'file',
                'version': 'ver0',
            },
            {
                'package': 'pkg1',
                'license_short': ['short'],
                'license_long': ['long', 'another license'],
                'file': 'file',
                'version': 'ver0',
            },
            {
                'package': 'pkg2',
                'license_short': ['short'],
                'license_long': ['another license'],
                'file': 'file',
                'version': 'ver1',
            },
        ]
        licenses_allowed = ['another license']
        errors = [pkgs[0], pkgs[1]]
        self.assertEqual(api.check_licenses(pkgs, licenses_allowed), errors)

        # Test multiple licenses.
        pkgs = [
            {
                'package': 'pkg0',
                'license_short': ['short'],
                'license_long': ['long', 'another', 'more'],
                'file': 'file',
                'version': 'ver0',
            },
        ]
        licenses_allowed = ['another', 'more']
        errors = [pkgs[0]]
        self.assertEqual(api.check_licenses(pkgs, licenses_allowed), errors)

        pkgs = [
            {
                'package': 'pkg0',
                'license_short': ['short'],
                'license_long': ['more'],
                'file': 'file',
                'version': 'ver0',
            },
        ]
        licenses_allowed = ['another', 'more']
        errors = list()
        self.assertEqual(api.check_licenses(pkgs, licenses_allowed), errors)

        # Test empty license_long value.
        pkgs = [
            {
                'package': 'pkg0',
                'license_short': ['short'],
                'license_long': [],
                'file': 'file',
                'version': 'ver0',
            },
        ]
        licenses_allowed = ['another', 'more']
        errors = [pkgs[0]]
        self.assertEqual(api.check_licenses(pkgs, licenses_allowed), errors)

        # Test if invalid common data structure function is called.
        m = unittest.mock.Mock(return_value=False)
        with patch('licheck.api.check_data_object_structure', m):
            with self.assertRaises(exceptions.InvalidCommonDataStructure):
                api.check_licenses(list(), list())

    def test_prepare_print(self):
        r"""Test test_prepare_print."""
        # Test normal packages.
        pkgs = [
            {
                'package': 'pkg0',
                'license_short': ['shortshortshortshortshortshortshortshortshortshortshortshortshortshortshort'],
                'license_long': ['abcd'],
                'file': 'file',
                'version': 'ver0',
            },
        ]
        self.assertEqual(api.prepare_print(pkgs, False), pkgs)

        # Test empty packages.
        pkgs = list()
        self.assertEqual(api.prepare_print(pkgs, False), list())

        # Test cut_output for license short.
        pkgs = [
            {
                'package': 'pkg1',
                'license_short': ['shortshortshortshortshortshortshortshortshortshortshortshortshortshortshort'],
                'license_long': ['abcd'],
                'file': 'file',
                'version': 'ver1',
            },
        ]
        pkgs_cut = [
            {
                'package': 'pkg1',
                'license_short': ['shortshortshorts...'],
                'license_long': ['abcd'],
                'file': 'file',
                'version': 'ver1',
            },
        ]
        self.assertEqual(api.prepare_print(pkgs, True), pkgs_cut)

        # Test cut_output for license long.
        pkgs = [
            {
                'package': 'pkg1',
                'license_short': ['abcd'],
                'license_long': ['shortshortshortshortshortshortshortshortshortshortshortshortshortshortshort'],
                'file': 'file',
                'version': 'ver1',
            },
        ]
        pkgs_cut = [
            {
                'package': 'pkg1',
                'license_short': ['abcd'],
                'license_long': ['shortshortshorts...'],
                'file': 'file',
                'version': 'ver1',
            },
        ]
        self.assertEqual(api.prepare_print(pkgs, True), pkgs_cut)

        # Test empty version.
        pkgs = [
            {
                'package': 'pkg1',
                'license_short': ['abcd'],
                'license_long': ['shortshortshortshortshortshortshortshortshortshortshortshortshortshortshort'],
                'file': 'file',
                'version': str(),
            },
        ]
        pkgs_fixed = copy.deepcopy(pkgs)
        pkgs_fixed[0]['version'] = '-'
        self.assertEqual(api.prepare_print(pkgs, False), pkgs_fixed)

        # Test if invalid common data structure function is called.
        m = unittest.mock.Mock(return_value=False)
        with patch('licheck.api.check_data_object_structure', m):
            with self.assertRaises(exceptions.InvalidCommonDataStructure):
                api.prepare_print(list(), False)

    def test_get_binary_and_program(self):
        r"""Test get_binary_and_program."""
        # Test python.
        m = unittest.mock.Mock(return_value='/some/path/deplic')
        with patch('shutil.which', m):
            self.assertEqual(api.get_binary_and_program('python'), ('/some/path/deplic', 'dep_license'))

        # Test another.
        m = unittest.mock.Mock(return_value=None)
        with patch('shutil.which', m):
            with self.assertRaises(exceptions.BinaryDoesNotExist):
                api.get_binary_and_program('another')

        # Test empty.
        # No need to mock since the binary is: ''
        with self.assertRaises(exceptions.BinaryDoesNotExist):
            api.get_binary_and_program(str())

    @unittest.skip("empty test")
    def test_print_errors(self):
        r"""Test print_errors."""

    def test_check_configuration_structure(self):
        r"""Test the configuration structure."""
        ##############
        # Test local #
        ##############

        # Test a valid data structure.
        data_struct = {
            'language': 'python',
            'include': list(),
            'files to check': ['a'],
            'allowed licenses': list(),
        }
        self.assertTrue(api.check_configuration_structure(data_struct, True))

        # Test a valid data structure with more data.
        data_struct = {
            'language': 'python',
            'include': ['https://a.b.com', 'https://c.d.com'],
            'files to check': ['a', 'b', 'c'],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertTrue(api.check_configuration_structure(data_struct, True))

        # Test invalid combinations.
        # Test missing values.
        data_struct = {
            'include': ['https://a.b.com', 'http://c.d.com'],
            'files to check': ['a', 'b', 'c'],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        data_struct = {
            'language': 'python',
            'files to check': ['a', 'b', 'c'],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        data_struct = {
            'language': 'python',
            'include': ['https://a.b.com', 'http://c.d.com'],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        data_struct = {
            'language': 'python',
            'include': ['https://a.b.com', 'http://c.d.com'],
            'files to check': ['a', 'b', 'c'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))

        # Test wrong data types.
        # Integer.
        data_struct = {
            'language': 1,
            'include': ['https://a.b.com', 'http://c.d.com'],
            'files to check': ['a', 'b', 'c'],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        # No list.
        data_struct = {
            'language': 'python',
            'include': 'https://a.b.com',
            'files to check': ['a', 'b', 'c'],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        # Integer list.
        data_struct = {
            'language': 'python',
            'include': [1, 2],
            'files to check': ['a', 'b', 'c'],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        # No list.
        data_struct = {
            'language': 'python',
            'include': ['a', 'b'],
            'files to check': 'a',
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        # Integer list.
        data_struct = {
            'language': 'python',
            'include': ['a', 'b'],
            'files to check': [1, 2],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        # No list.
        data_struct = {
            'language': 'python',
            'include': ['a', 'b'],
            'files to check': ['a'],
            'allowed licenses': 'd',
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        # Integer list.
        data_struct = {
            'language': 'python',
            'include': ['a', 'b'],
            'files to check': ['a'],
            'allowed licenses': [1, 2],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))

        # Test empty data.
        data_struct = {
            'language': str(),
            'include': ['https://a.b.com', 'http://c.d.com'],
            'files to check': ['a', 'b', 'c'],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))
        data_struct = {
            'language': 'python',
            'include': ['https://a.b.com', 'http://c.d.com'],
            'files to check': [],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))

        # Test URL scheme.
        data_struct = {
            'language': 'python',
            'include': ['ssh://a.b.com', 'http://c.d.com'],
            'files to check': ['a', 'b', 'c'],
            'allowed licenses': ['d', 'e', 'f'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, True))

        ###############
        # Test remote #
        ###############

        # Test a valid data structure.
        data_struct = {
            'language': 'python',
            'include': list(),
            'files to check': list(),
            'allowed licenses': list(),
        }
        self.assertTrue(api.check_configuration_structure(data_struct, False))

        # Test a valid data structure with more data.
        data_struct = {
            'language': 'python',
            'include': list(),
            'files to check': list(),
            'allowed licenses': ['a', 'b', 'c'],
        }
        self.assertTrue(api.check_configuration_structure(data_struct, False))

        # Test invalid combinations.
        # Test missing values.
        data_struct = {
            'include': list(),
            'files to check': list(),
            'allowed licenses': ['a', 'b', 'c'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))
        data_struct = {
            'language': 'python',
            'files to check': list(),
            'allowed licenses': ['a', 'b', 'c'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))
        data_struct = {
            'language': 'python',
            'include': list(),
            'allowed licenses': ['a', 'b', 'c'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))
        data_struct = {
            'language': 'python',
            'include': list(),
            'files to check': list(),
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))

        # Test wrong data types.
        # Integer.
        data_struct = {
            'language': 1,
            'include': list(),
            'files to check': list(),
            'allowed licenses': ['a', 'b', 'c'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))
        # No list.
        data_struct = {
            'language': 'python',
            'include': str(),
            'files to check': list(),
            'allowed licenses': ['a', 'b', 'c'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))
        # No list.
        data_struct = {
            'language': 'python',
            'include': list(),
            'files to check': str(),
            'allowed licenses': ['a', 'b', 'c'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))
        # No list.
        data_struct = {
            'language': 'python',
            'include': list(),
            'files to check': list(),
            'allowed licenses': 'c',
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))
        # Integer list.
        data_struct = {
            'language': 'python',
            'include': list(),
            'files to check': list(),
            'allowed licenses': [1, 2, 3],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))

        # Test unexpected values.
        # Remote files need to have some empty lists.
        data_struct = {
            'language': 'python',
            'include': str(),
            'files to check': list(),
            'allowed licenses': ['a', 'b', 'c'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))
        data_struct = {
            'language': 'python',
            'include': list(),
            'files to check': str(),
            'allowed licenses': ['a', 'b', 'c'],
        }
        self.assertFalse(api.check_configuration_structure(data_struct, False))

    def test_check_cache_structure(self):
        r"""Test the cache data structure."""
        # Test a valid data structure.
        data_struct = {
            'hash0': [
                {
                    'p': 'pkg0',
                    'l': ['a', 'b', 'c', 'd'],
                    's': ['d'],
                    'v': str(),
                }
            ],
        }
        self.assertTrue(api.check_cache_structure(data_struct))

        # Test a valid data structure with more data.
        data_struct = {
            'hash0': [
                {
                    'p': 'pkg0',
                    'l': ['a', 'b', 'c', 'd'],
                    's': ['d'],
                    'v': 'a version',
                }
            ],
        }
        self.assertTrue(api.check_cache_structure(data_struct))

        # Test invalid combinations.
        # Test missing values.
        data_struct = {
            'hash0': [
                {
                    'l': ['a', 'b', 'c', 'd'],
                    's': ['d'],
                    'v': 'a version',
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))
        data_struct = {
            'hash0': [
                {
                    'p': 'pkg0',
                    's': ['d'],
                    'v': 'a version',
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))
        data_struct = {
            'hash0': [
                {
                    'p': 'pkg0',
                    'l': ['a', 'b', 'c', 'd'],
                    'v': 'a version',
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))
        data_struct = {
            'hash0': [
                {
                    'p': 'pkg0',
                    'l': ['a', 'b', 'c', 'd'],
                    's': ['d'],
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))

        # Test wrong data types.
        # Integer.
        data_struct = {
            'hash0': [
                {
                    'p': 1,
                    'l': ['a', 'b', 'c', 'd'],
                    's': ['d'],
                    'v': 'a version',
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))
        # No list.
        data_struct = {
            'hash0': [
                {
                    'p': 'pkg0',
                    'l': 'a',
                    's': ['d'],
                    'v': 'a version',
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))
        # Integer list.
        data_struct = {
            'hash0': [
                {
                    'p': 'pkg0',
                    'l': [1, 2, 3, 4],
                    's': ['d'],
                    'v': 'a version',
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))
        # No list.
        data_struct = {
            'hash0': [
                {
                    'p': 'pkg0',
                    'l': ['a', 'b', 'c', 'd'],
                    's': 'd',
                    'v': 'a version',
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))
        # Integer list.
        data_struct = {
            'hash0': [
                {
                    'p': 'pkg0',
                    'l': ['a', 'b', 'c', 'd'],
                    's': [1],
                    'v': 'a version',
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))

        # Test unexpected values.
        data_struct = {
            'hash0': [
                {
                    'p': str(),
                    'l': ['a', 'b', 'c', 'd'],
                    's': ['e', 'f', 'g'],
                    'v': 'a version',
                }
            ],
        }
        self.assertFalse(api.check_cache_structure(data_struct))

    def test_check_data_object_structure(self):
        r"""Test."""
        # Test a valid data structure.
        data_struct = [
            {
                'package': 'requests',
                'license_short': list(),
                'license_long': ['a'],
                'file': 'a',
                'version': str(),
            }
        ]
        self.assertTrue(api.check_data_object_structure(data_struct))

        # Test a valid data structure with more data.
        data_struct = [
            {
                'package': 'requests',
                'license_short': list(),
                'license_long': ['a'],
                'file': 'a',
                'version': '0.1',
            }
        ]
        self.assertTrue(api.check_data_object_structure(data_struct))

        # Test invalid combinations.
        # Test missing values.
        data_struct = [
            {
                'license_short': list(),
                'license_long': ['a'],
                'file': 'a',
                'version': str(),
            }
        ]
        self.assertFalse(api.check_data_object_structure(data_struct))
        data_struct = [
            {
                'package': 'requests',
                'license_long': ['a'],
                'file': 'a',
                'version': str(),
            }
        ]
        self.assertFalse(api.check_data_object_structure(data_struct))
        data_struct = [
            {
                'package': 'requests',
                'license_short': list(),
                'file': 'a',
                'version': str(),
            }
        ]
        self.assertFalse(api.check_data_object_structure(data_struct))
        data_struct = [
            {
                'package': 'requests',
                'license_short': list(),
                'license_long': ['a'],
                'version': str(),
            }
        ]
        self.assertFalse(api.check_data_object_structure(data_struct))
        data_struct = [
            {
                'package': 'requests',
                'license_short': list(),
                'license_long': ['a'],
                'file': 'a',
            }
        ]
        self.assertFalse(api.check_data_object_structure(data_struct))

        # Test wrong data types.
        # Integer.
        data_struct = [
            {
                'package': 1,
                'license_short': list(),
                'license_long': ['a'],
                'file': 'a',
                'version': str(),
            }
        ]
        self.assertFalse(api.check_data_object_structure(data_struct))
        # No list.
        data_struct = [
            {
                'package': 'requests',
                'license_short': 'a',
                'license_long': ['a'],
                'file': 'a',
                'version': str(),
            }
        ]
        self.assertFalse(api.check_data_object_structure(data_struct))
        # Integer list.
        data_struct = [
            {
                'package': 'requests',
                'license_short': [1, 2, 3],
                'license_long': ['a'],
                'file': 'a',
                'version': str(),
            }
        ]
        self.assertFalse(api.check_data_object_structure(data_struct))
        # No list.
        data_struct = [
            {
                'package': 'requests',
                'license_short': list(),
                'license_long': 'a',
                'file': 'a',
                'version': str(),
            }
        ]
        self.assertFalse(api.check_data_object_structure(data_struct))
        # Integer list.
        data_struct = {
            'package': 'requests',
            'license_short': list(),
            'license_long': [1, 2, 3],
            'file': 'a',
            'version': str(),
        }
        self.assertFalse(api.check_data_object_structure(data_struct))
        # Integer.
        data_struct = {
            'package': 'requests',
            'license_short': list(),
            'license_long': ['a'],
            'file': 1,
            'version': str(),
        }
        self.assertFalse(api.check_data_object_structure(data_struct))
        # Integer.
        data_struct = {
            'package': 'requests',
            'license_short': list(),
            'license_long': ['a'],
            'file': 'a',
            'version': 1,
        }
        self.assertFalse(api.check_data_object_structure(data_struct))

        # Test empty data.
        data_struct = {
            'package': str(),
            'license_short': list(),
            'license_long': ['a'],
            'file': 'a',
            'version': str(),
        }
        self.assertFalse(api.check_data_object_structure(data_struct))
        data_struct = {
            'package': 'requests',
            'license_short': list(),
            'license_long': ['a'],
            'file': str(),
            'version': str(),
        }
        self.assertFalse(api.check_data_object_structure(data_struct))

    @unittest.skip("empty test")
    def test_read_configuration_file(self):
        r"""Test."""

    @unittest.skip("empty test")
    def test_read_cache_file(self):
        r"""Test."""

    def test_read_remote_files(self):
        r"""Test."""
        # Test a valid data structure.
        # File does not exists.
        hash_value = '1234abcd'
        include_files = ['http://example.com/1.yaml']
        remote_file_content = '''\
allowed licenses:
    - OSI Approved::Apache Software License
    - OSI Approved::MIT License
    - OSI Approved::GNU General Public License v3 or later (GPLv3+)
    - OSI Approved::BSD License

files to check: []

language: python

include: []
        '''
        expected = ['OSI Approved::Apache Software License', 'OSI Approved::MIT License', 'OSI Approved::GNU General Public License v3 or later (GPLv3+)', 'OSI Approved::BSD License']
        with requests_mock.Mocker() as mock_requests:
            with patch('hashlib.sha512') as mock_hashlib:
                with patch('builtins.open', unittest.mock.mock_open()) as mock_open:
                    mock_requests.get(include_files[0], text=remote_file_content)
                    instance = mock_hashlib.return_value
                    instance.hexdigest.return_value = hash_value
                    with patch('licheck.api.read_configuration_file', return_value=(expected, list(), str(), list())):
                        # Sorting is not guaranteed because of the sue of a set.
                        self.assertCountEqual(api.read_remote_files(include_files, '.cache'), expected)
        handle = mock_open()
        handle.write.assert_called_once_with(remote_file_content.encode('UTF-8'))

        # File exists.
        hash_value = '1234abcd'
        include_files = ['http://example.com/1.yaml']
        expected = ['OSI Approved::Apache Software License', 'OSI Approved::MIT License', 'OSI Approved::GNU General Public License v3 or later (GPLv3+)', 'OSI Approved::BSD License']
        with patch('hashlib.sha512') as mock_hashlib:
            with patch('pathlib.Path') as mock_pathlib:
                instance = mock_hashlib.return_value
                instance.hexdigest.return_value = hash_value
                instance = mock_pathlib.return_value
                instance.is_file.return_value = True
                with patch('licheck.api.read_configuration_file', return_value=(expected, list(), list(), list())):
                    # sorting is not guaranteed because of the sue of a set.
                    self.assertCountEqual(api.read_remote_files(include_files, '.cache'), expected)
        handle = mock_open()
        handle.write.assert_called_once_with(remote_file_content.encode('UTF-8'))

        # TODO: check if IncoherentProgrammingLanguageValue is raised correctly.

    def test_create_dependencies_files_data_structure(self):
        r"""Test."""
        # Test a valid data structure.
        files_to_check = ['a', 'b', 'c']
        file_data = 'testing\ntesting\n'
        hash_value = '1234abcd'

        with patch('hashlib.sha512') as mock:
            with patch('builtins.open', unittest.mock.mock_open(read_data=file_data)):
                instance = mock.return_value
                instance.hexdigest.return_value = hash_value
                result = api.create_dependencies_files_data_structure(files_to_check)
                self.assertEqual(result, {'a': hash_value, 'b': hash_value, 'c': hash_value})

        # Test an invalid data structure.
        files_to_check = ['a', 'b', 2]
        with self.assertRaises(TypeError):
            api.create_dependencies_files_data_structure(files_to_check)

    @unittest.skip("empty test")
    def test_pipeline(self):
        r"""Test."""


if __name__ == '__main__':
    unittest.main()
